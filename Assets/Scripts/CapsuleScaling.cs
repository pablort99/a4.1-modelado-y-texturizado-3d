﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CapsuleScaling : MonoBehaviour
{
    [SerializeField]
    private Vector3 axes; //posibles ejes de escalado
    public float scaleUnits; //velocidad de escalado

    private void Update()
    {
        //acotacion de los valores de escalada al valor unitario [1,1]
        axes = CapsuleMovement.ClampVector3(axes);

        transform.localScale += axes * (scaleUnits * Time.deltaTime);
    }
}
